package main

import "testing"

//test for question 1
func TestArraySort(t *testing.T) {
	array := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	correctArray := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"}
	testArray := ArraySort(array)

	for i, v := range correctArray {
		if v != testArray[i] {
			t.Error("Arrays are not same")
		}
	}
}

//test for question 3
func TestRepeatCount(t *testing.T) {
	array := []string{"apple", "pie", "apple", "red", "red", "red"}
	correctResult := "red"
	testArray := RepeatCount(array)

	if testArray != correctResult {
		t.Error("Strings are not same")
	}
}
