package main

import (
	"fmt"
	"strings"
)

//question 1
func ArraySort(array []string) []string {
	for i := 0; i < len(array)-1; i++ {
		for j := 0; j < len(array)-i-1; j++ {
			aCount1 := strings.Count(array[j], "a")
			aCount2 := strings.Count(array[j+1], "a")
			if aCount1 < aCount2 {
				array[j], array[j+1] = array[j+1], array[j]
				for z := 0; z < len(array)-j-1; z++ {
					aCount1 := strings.Count(array[z], "a")
					aCount2 := strings.Count(array[z+1], "a")
					if aCount1 < aCount2 {
						array[z], array[z+1] = array[z+1], array[z]
					}
				}

			}
			if aCount1 == aCount2 {
				len1 := len(array[j])
				len2 := len(array[j+1])
				if len1 < len2 {
					array[j], array[j+1] = array[j+1], array[j]
				}
			}
		}
	}
	return array
}

//qusetion 2

func Rec(n int) int {
	if n > 2 {
		Rec(n / 2)
	}
	fmt.Println(n)
	return 1
}

//question 3
func RepeatCount(array []string) string {
	finalValue := ""
	finalCount := 0
	for i := 0; i < len(array); i++ {
		value := array[i]
		count := 0
		for j := 0; j < len(array); j++ {
			if array[i] == array[j] {
				count++
			}
		}
		if count >= finalCount {
			finalCount = count
			finalValue = value
		}

	}
	//fmt.Println("final value and count is=", finalValue, "-", finalCount)
	return finalValue
}

func main() {
	fmt.Println("Go test")
	Rec(9)
}
