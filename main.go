package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv" //string converter

	"github.com/gorilla/mux"
)

//User struct model
type User struct {
	ID   string "json:'id'"
	Name string "json:'name'"
}

//init user var as a slice User struct
var users []User

//Get all Users
func getAllUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*") //this is for allowing cross origin.otherwise browser will block it
	//header value of content type. without this it will served as text
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

//get single user via id
func getSingleUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*") //this is for allowing cross origin.otherwise browser will block it
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) //get parameters with mux router
	//loop through users and find with id
	for _, item := range users {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&User{})
}

//create a new user
func createUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*") //this is for allowing cross origin.otherwise browser will block it
	w.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	user.ID = strconv.Itoa(rand.Intn(10000000)) // mock id, generate random id (could generate same id, its not safe)
	users = append(users, user)
	json.NewEncoder(w).Encode(users)
}

//update user
func updateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*") //this is for allowing cross origin.otherwise browser will block it
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range users {
		if item.ID == params["id"] {
			users = append(users[:index], users[index+1:]...)
			var user User
			_ = json.NewDecoder(r.Body).Decode(&user)
			user.ID = params["id"]
			users = append(users, user)
			json.NewEncoder(w).Encode(user)
			return
		}
	}
	json.NewEncoder(w).Encode(users)
}

//delete a user with id
func deleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*") //this is for allowing cross origin.otherwise browser will block it
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range users {
		if item.ID == params["id"] {
			users = append(users[:index], users[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(users)
}

func main() {
	//Init router
	r := mux.NewRouter()

	//Mock Data -
	users = append(users, User{ID: "1", Name: "Huseyin Hilal"})
	users = append(users, User{ID: "2", Name: "John Doe"})
	//Route handlers / endpoints
	r.HandleFunc("/api/getall", getAllUsers).Methods("GET", "OPTIONS")
	r.HandleFunc("/api/get/{id}", getSingleUser).Methods("GET", "OPTIONS")
	r.HandleFunc("/api/create", createUser).Methods("POST", "OPTIONS")
	r.HandleFunc("/api/update/{id}", updateUser).Methods("PUT", "OPTIONS")
	r.HandleFunc("/api/delete/{id}", deleteUser).Methods("DELETE", "OPTIONS")
	log.Fatal(http.ListenAndServe(":8000", r))

}
