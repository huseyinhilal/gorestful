import React,{Component, useEffect,useState} from "react";
import axios from "axios";


//out base url - base endpoint
const api = axios.create({
  baseURL:'http://localhost:3000/api'
})

function App() {
  const [users,setUser]=useState([]);
  const [userId,setUserId]=useState("");
  const [userName,setUserName]=useState("");
  useEffect(()=>{
    fetch('http://localhost:8000/api/getall').then((result)=>{result.json().then((resp)=>{setUser(resp) } ) }) },[])
 
    //create new user
    function saveData(){
      let article = { Name: userName }
      axios.post('http://localhost:8000/api/create', JSON.stringify(article))
        .then(window.location.reload());
    }

    //delete user
    function deleteData(){
      axios.delete('http://localhost:8000/api/delete/'+userId)
        .then(window.location.reload());
    }

    //update user
    function updateData(){
      let article = { Name: userName }
      axios.put('http://localhost:8000/api/update/'+userId, JSON.stringify(article))
        .then(window.location.reload());
    }

  return (
    <div className='App'>
      <div>
    <input type="text" placeholder='ID' name="id" value={userId} onChange={(e)=>{setUserId(e.target.value)}}></input>
    
      </div>
      <div>
      <input type="text" placeholder='Name' name="name" value={userName} onChange={(e)=>{setUserName(e.target.value)}}></input>
    
      </div>
      
      <button type='button' onClick={saveData}>New</button>
      <button type='button' onClick={updateData}>Update</button>
      <button type='button' onClick={deleteData}>Delete</button>
     
      <div>
      <table border="1">
       <tbody>
       <tr>
          <td>ID</td>
          <td>Name</td>
        </tr>
        {users.map((item,i)=>
          <tr key={i}>
          <td>{item.ID}</td>
          <td>{item.Name}</td>
        </tr>
        )}
       </tbody>
      </table>
      </div>
      
    </div>

  );
}

export default App;

